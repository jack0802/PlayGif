package com.jack.gifresearch;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.LinearLayout.LayoutParams;

public class MainActivity extends Activity {

	private GifView gif;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		gif = (GifView) findViewById(R.id.gif);  
        // ���ñ���gifͼƬ��Դ  
        gif.setMovieResource(R.raw.test); 
        gif.setLayoutParams(new LayoutParams(1280, 752));
		
	}
}
